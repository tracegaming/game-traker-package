# Trace GameTracker Server Wrapper package
### This npm package is just a REST API wrapper for Trace GameTracker server to provide the same functionality as the API
> Note: Internal and private package, must be used only for internal testing or production inside Trace Gaming organization, do not redistribute

---

## Prerequisites for installation
1. Access to game-traker-package repository located on https://bitbucket.org/tracegaming/game-traker-package/src/master/
2. Registration of SSH key of development or production machine on repository configuration: https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

---

## Installation
1. Execute the following npm command: `npm install git+ssh://bitbucket.org:tracegaming/game-traker-package.git`

---

## Implementation
1. Import game-traker-package module: `var tracker = require('game-tracker');`
2. Initialize external server parameters: `tracker(SERVER_URL);`