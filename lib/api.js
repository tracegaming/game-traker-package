const urllib = require('urllib');

GameTracker.SERVER_URL = "";

function GameTracker(serverURL){

    GameTracker.SERVER_URL = serverURL;
}

GameTracker.services = new Services();

function Services(){
    var endPointURL = "/services";

    this.Games = {
        Warzone: 1
    };

    this.WinningCriteria = {
        Position: 0,
        Kills: 1
    };

    this.StatsType = {
        Summary: 0,
        Extended: 1
    };

    this.Filters = {
        WZTriosBRMode: {"mode": "br_brtrios"},
        WZDuosBRMode: {"mode": "br_brduos"},
        WZSoloBRMode: {"mode": "br_vg_royale_solo"},
        WZSquadsBRMode: {"mode": "br_brquads"},
    }

    this.SummaryFieldsOperators = {
        sum: "sum",
        plane: "plane",
    }

    /**
     * Verifies access to public data from the provided gamertag on the specified platform
     * @param {string} gamerTag User gamertag, Activision platform is not supported
     * @param {Games} game Service or game 
     * @returns Request result
     */
    this.userDataAccess = async (gamerTag, game) => {
        const operation = "/user/data/access";
        const url = GameTracker.SERVER_URL + endPointURL + operation;
        return await sendRequestToServer(url, 'POST', {
            gamerTag: gamerTag,
            game: game
        });
    };

    /**
     * Gets public data from the provided gamertag on the specified platform
     * @param {string} gamerTag User gamertag, Activision platform is not supported
     * @param {Games} game Service or game 
     * @param {StatsType} type Stats type
     * @returns Request result
     */
    this.userDataStats = async (gamerTag, game, type, forceCacheRefresh) => {
        const operation = "/user/data/stats";
        const url = GameTracker.SERVER_URL + endPointURL + operation;
        return await sendRequestToServer(url, 'POST', {
            gamerTag: gamerTag,
            game: game,
            type: type
        }, forceCacheRefresh);
    };

    /**
     * Retrieves the most recent match from the service platform and compares the results data from the specified gamertags to determine a position
     * @param {[string]} gamerTags User gamertags which will be used to determine positions
     * @param {Game} game Service or game
     * @param {WinningCriteria} criteria Winning criteria
     * @returns Request result
     */
    this.matchRecentResults = async (gamerTags, game, criteria) => {
        const operation = "/match/recent/results";
        const url = GameTracker.SERVER_URL + endPointURL + operation;
        let res = await sendRequestToServer(url, 'POST', {
            gamerTags: gamerTags,
            game: game,
            criteria: criteria
        });
        return res;
    };


    /**
     * Retrieves the most recent matches from the service platform and compares the results data from the specified gamertags to determine a position
     * @param {[string]} gamerTags User gamertags which will be used to determine positions
     * @param {Game} game Service or game
     * @param {[Filters]} filters Filters for matches
     * @param {[{operator: SummaryFieldsOperators, field: string}]} summaryFields summary fields
     * @returns Request result
     */
     this.matchRecentHistory = async (gamerTags, game, filters, summaryFields) => {
        const operation = "/match/recent/history";
        const url = GameTracker.SERVER_URL + endPointURL + operation;
        let res = await sendRequestToServer(url, 'POST', {
            gamerTags,
            game,
            filters,
            summaryFields
        });
        return res;
    };
}

var sendRequestToServer = async (url, method, data, forceCacheRefresh) => {
    const urlDestination = url;
    try{
        let result = await urllib.request(urlDestination,{
            contentType: "json",
            dataType: "json",
            data: data,
            method: method,
            timeout: 120000,
            headers: (forceCacheRefresh != undefined && forceCacheRefresh) ? {
                    'Cache-Control': 'no-cache'
            } : {}
        });
        if(result.res.statusCode != 200){
            return {success: false, error: result.data != null ? result.data.error : null, errors: result.data != null ? result.data.errors : null};
        }
        return {success: true, data: result.data.response};
    }catch(err){
        return {success: false, error: err, errors: err.message};
    }
}

module.exports = GameTracker;