var assert = require('assert');
var GameTracker = require('../lib/api');

GameTracker("localhost:3000");

describe('Services', function () {
    describe('User data', function () {
        it('Access', function (done) {
            GameTracker.services.userDataAccess("xSmithAK47x", GameTracker.services.Games.Warzone).then(res => {
                assert.strictEqual(res.data, true);
                done();
            });
        });
    });
    describe('User data', function () {
        it('Stats Summary', function (done) {
            GameTracker.services.userDataStats("xSmithAK47x", GameTracker.services.Games.Warzone, GameTracker.services.StatsType.Summary).then(res => {
                assert.strictEqual(res.data.username, 'xSmithAK47x');
                done();
            });
        });
        it('Stats Extended', function (done) {
            GameTracker.services.userDataStats("xSmithAK47x", GameTracker.services.Games.Warzone, GameTracker.services.StatsType.Extended).then(res => {
                assert.strictEqual(res.data.username, 'xSmithAK47x');
                done();
            });
        });
    });
    describe('Match data', function () {
        it('Results', function (done) {
            GameTracker.services.matchRecentResults(["xSmithAK47x", "xSmithAK47x8879"], GameTracker.services.Games.Warzone, GameTracker.services.WinningCriteria.Kills).then(res => {
                assert.notStrictEqual(res.success, false);
                done();
            });
        });
    });

    describe('Match History', function () {
        it('Results', function (done) {
            GameTracker.services.matchRecentHistory(
                ["GoodToGo#11567"], 
                GameTracker.services.Games.Warzone, 
                [GameTracker.services.Filters.WZSoloBRMode], 
                [{operator: GameTracker.services.SummaryFieldsOperators.sum, field: "kills"}, {operator: GameTracker.services.SummaryFieldsOperators.plane, field: "teamPlacement"}]).then(res => {
                assert.notStrictEqual(res.success, false);
                done();
            });
        });
    });
});